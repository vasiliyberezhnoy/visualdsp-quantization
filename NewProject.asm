
#include "def21060.h"

#define N 4				
#define NDel 4.0		  
#define TimerPeriod	15 


.SECTION/DM     dm_ports;
.VAR res_in;
.VAR res_out;


.SECTION/PM     pm_rsti;           
		nop;
		jump start;
		nop;
		nop;

.SECTION/PM     timer_hi;               
        jump ComputeProc (DB);	
        F3 = DM(res_in);		
        F4 = F4 + F3;				
		nop;

.SECTION/PM     pm_code;
start:

		R0 = 0x00000010;  
		DM(SYSCON) = R0;
		R0 = 0x20108421; 
		DM(WAIT) = R0;

		R1 = N;
		F2 = NDel;
		F2 = RECIPS F2;			
		F5 = 0.0;

		
		TPeriod = TimerPeriod;
		TCount = TimerPeriod;
		
		bit set MODE1 IRPTEN;	
		bit set MODE2 TIMEN;	
		bit set IMASK TMZHI;	
wait:   idle;
    	jump wait;				

ComputeProc:      
		R1 = R1 - 1;
		if ne jump exit_proc;	
		F0 = F4 * F2, F4 = F5;	 
		DM(res_out) = F0;		
 		R1 = N;
exit_proc:
		rti;
